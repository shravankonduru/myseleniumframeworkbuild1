package resources;

import java.io.IOException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;

import com.aventstack.extentreports.Status;


public class listeners implements ITestListener{
	public static Logger log = LogManager.getLogger(listeners.class.getName());
	
	base baseObj = new base();

	public void onTestStart(ITestResult result) {
		// TODO Auto-generated method stub
		
	}

	public void onTestSuccess(ITestResult result) {
		// TODO Auto-generated method stub
		base.extTest.log(Status.PASS, "TEST CASE PASSED" + result.getName()); //to add Name in Extent Report
		log.debug(result.getName()+ " passed");
	}

	public void onTestFailure(ITestResult result) {
		 //TODO Auto-generated method stub
		log.debug("Enter onTestFailure Method");
		String screenshotPath = null;
			base.extTest.log(Status.FAIL, "TEST CASE FAILED is " + result.getName()); //to add Name in Extent Report
			base.extTest.log(Status.FAIL, "TEST CASE FAILED is " + result.getThrowable()); //to add Name in Extent Report
			
			try {
			 screenshotPath = base.getScreenshot(result.getName());
			 base.extTest.addScreenCaptureFromPath(screenshotPath);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				base.extTest.fail(e);
				log.fatal("call to base.getScreenShot method failed. Test Name : "+result.getName());
				log.fatal("call to base.getScreenShot method failed. Exceptions : "+result.getThrowable());
			}	
		
		log.debug("Exit onTestFailure Method");
	}

	public void onTestSkipped(ITestResult result) {
		// TODO Auto-generated method stub
		base.extTest.log(Status.SKIP, "TEST CASE IS SKIPPED" + result.getName()); //to add Name in Extent Report
		log.debug(result.getName()+ " Skipped");
	}

	public void onTestFailedButWithinSuccessPercentage(ITestResult result) {
		// TODO Auto-generated method stub
		
	}

	public void onStart(ITestContext context) {
		// TODO Auto-generated method stub
		
	}

	public void onFinish(ITestContext context) {
		// TODO Auto-generated method stub
		
	}
	
	/**
	public void onTestFailure(ITestResult result) {
		 //TODO Auto-generated method stub
		log.debug("Enter onTestFailure Method");
		try {
			baseObj.getScreenshots(result.getName());
		log.fatal("Test Failed. Screenshot is taken");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			log.fatal("Exception occured while taking screenshot. Shortened Error message: "
			+e.getCause().toString().substring(100));
			e.printStackTrace();
		}
		
		// for future use.
		String screenshotPath = null;
		if(result.getStatus() == ITestResult.FAILURE) {
			base.extTest.log(Status.FAIL, "TEST CASE FAILED is " + result.getName()); //to add Name in Extent Report
			base.extTest.log(Status.FAIL, "TEST CASE FAILED is " + result.getThrowable()); //to add Name in Extent Report
			
			try {
			 screenshotPath = base.getScreenshot(result.getName());
			 base.extTest.addScreenCaptureFromPath(screenshotPath);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				log.fatal("call to base.getScreenShot method failed. Test Name : "+result.getName());
				log.fatal("call to base.getScreenShot method failed. Exceptions : "+result.getThrowable());
			}	
		}
		else if (result.getStatus() == ITestResult.SKIP){
			base.extTest.log(Status.SKIP, "TEST CASE IS SKIPPED" + result.getName()); //to add Name in Extent Report
		}
		else if (result.getStatus() == ITestResult.SUCCESS) {
			base.extTest.log(Status.PASS, "TEST CASE PASSED" + result.getName()); //to add Name in Extent Report
		}
		log.debug("Exit onTestFailure Method");
		
	}
	**/

}
