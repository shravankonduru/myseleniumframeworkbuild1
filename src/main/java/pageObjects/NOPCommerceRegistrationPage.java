package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class NOPCommerceRegistrationPage {
	
	public WebDriver pgdriver;
	
	public NOPCommerceRegistrationPage(WebDriver driver) {
		this.pgdriver=driver;
	}
	
	By RegistrationHeadingElement = By.xpath("//h1[contains(text(),'Registration')]");
	By FirstName = By.xpath("//*[contains(@name,'FirstName')]");
	By LastName = By.xpath("//*[contains(@name,'LastName')]");
	By Email = By.xpath("//*[contains(@name,'Email')]");
	By Country = By.xpath("//*[contains(@name,'Country')]");
	
	public WebElement getRegistrationHeadingElement()
	{
		return pgdriver.findElement(RegistrationHeadingElement);
	}
	
	
	public WebElement getFirstName()
	{
		return pgdriver.findElement(FirstName);
	}
	
	public WebElement getEmail()
	{
		return pgdriver.findElement(Email);
	}
	
	public WebElement getLastName()
	{
		return pgdriver.findElement(LastName);
	}
	
	public WebElement getCountry()
	{
		return pgdriver.findElement(Country);
	}
	
	
	
}
