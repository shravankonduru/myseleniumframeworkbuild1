package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class LandingPage {
	
	public WebDriver pgdriver;
	
	public LandingPage(WebDriver driver) {
		this.pgdriver=driver;
	}
	
	By signin = By.cssSelector("a[href*='sign_in']");
	By title = By.xpath("//h2[contains(text(),'Featured Courses')]");
	By navbar = By.xpath("//div[contains (@class,'navbar')]");
	
	public WebElement getLogin()
	{
		return pgdriver.findElement(signin);
	}
	
	public WebElement getTtile()
	{
		return pgdriver.findElement(title);
	}

	public WebElement getNavbar()
	{
		return pgdriver.findElement(navbar);
	}
}
