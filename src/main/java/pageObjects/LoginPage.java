package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class LoginPage {
	
	public WebDriver pgdriver;
	
	public LoginPage(WebDriver driver) {
		this.pgdriver=driver;
	}
	
	By email = By.xpath("//input[@name='user[email]']");
	By password = By.xpath("//input[@name='user[password]']");
	By loginBtn = By.xpath("//input[@value='Log In']");
	
	
	
	
	public WebElement getemail()
	{
		return pgdriver.findElement(email);
	}
	
	public WebElement getpassword()
	{
		return pgdriver.findElement(password);
	}
	
	public WebElement getloginBtn()
	{
		return pgdriver.findElement(loginBtn);
	}

}
