package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class NOPCommerceLandingPage {
	
	public WebDriver pgdriver;
	
	public NOPCommerceLandingPage(WebDriver driver) {
		this.pgdriver=driver;
	}
	
	By Login = By.xpath("//a[contains(text(),'Log in')]");
	By Register = By.xpath("//a[contains(text(),'Register')]");
	By LndPgValidaitonElement = By.xpath("//h1[contains(text(),'afaggedOpen-source & FREE')]");
	
	public WebElement getLogin()
	{
		return pgdriver.findElement(Login);
	}

	public WebElement getRegister()
	{
		return pgdriver.findElement(Register);
	}
	
	public WebElement getLndPgValidaitonElement()
	{
		return pgdriver.findElement(LndPgValidaitonElement);
	}
}
