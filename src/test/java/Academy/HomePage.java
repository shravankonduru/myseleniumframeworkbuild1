package Academy;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import pageObjects.LandingPage;
import pageObjects.LoginPage;
import pageObjects.NOPCommerceLandingPage;
import resources.base;

public class HomePage extends base {
	public static Logger log = LogManager.getLogger(HomePage.class.getName());
	
	@BeforeSuite
	public void setupextentobjs() {
		base.initializeExtentTest();
	}
	
	@AfterSuite
	public void flushupextentobjs() {
		base.EndReports();
	}

	@BeforeMethod
	public void initialize() throws IOException {
		log.info("********start of HomePage test 2**********");
		driver = initializeDriver();
		driver.get(prop.getProperty("url"));
		extTest = extReports.createTest("ValidateLogin");
	}

	@Test(dataProvider = "getData1")
	public void ValidateLogin(HashMap<String, String > data1) throws IOException {
//		driver = initializeDriver();
//		driver.get(prop.getProperty("url"));
		// one is inheritance
		// creating object to that class and invoke methods of it
		//extTest.createNode("Validate login with " + UserName);
		
		log.debug("******* Start ******Itereation for systemID: " + data1.get("System ID")+"**********");
		
		log.debug("System ID : "+data1.get("System ID"));
		log.debug("School : "+data1.get("School"));
		log.debug("School : "+data1.get("Phone"));
		log.debug("School : "+data1.get("Address"));
		log.debug("School : "+data1.get("City"));
		log.debug("School : "+data1.get("State"));
		log.debug("School : "+data1.get("Zip"));
		log.debug("School : "+data1.get("Type"));
		log.debug("School : "+data1.get("Principal"));
		log.debug("School : "+data1.get("Website"));
		log.debug("School : "+data1.get("Other"));
		log.debug("School : "+data1.get("Image"));
		log.debug("School : "+data1.get("Active"));
		log.debug("******* End ******Itereation for systemID: " + data1.get("System ID")+"**********");	
		
	//	extTest.createNode("Validate login with " +.pass("steps for new node");
		LandingPage lpobj = new LandingPage(driver);
		
	

		lpobj.getLogin().click();

//		LoginPage lp = new LoginPage(driver);
		NOPCommerceLandingPage lndPg = new NOPCommerceLandingPage(driver);

	//	lp.getemail().sendKeys(UserName);
	//	log.debug("provided  username" + UserName);
		
		extTest.pass("Username is entered successfully");
	//	lp.getpassword().sendKeys(Password);
		extTest.pass("password is entered successfully");
		log.debug("provided  Password");
		//lp.getloginBtn().click();
		log.debug("Click on Sing button");
	//	System.out.println("third text: " + text);
		log.info("inside homepage test");

	}

	/**
	@DataProvider
	public Object [][] getData() throws IOException {
		// Row stands for how many different data sets test should run
		// Column stands for how many values per each data set.
		
		String filepath = System.getProperty("user.dir")+"/src/main/java/resources/C2ImportSchoolSample.csv";
		
		
		ArrayList<HashMap<String, String >> data1 = base.getcsvData(filepath);
		
		/**
		Object[][] data = new Object[2][3];
		// 0th row
		data[0][0] = "nonrestricteduse@qw.com";
		data[0][1] = "abcd1234";
		data[0][2] = "nonrestricteduse@qw.com";
		// 1st row
		data[1][0] = "restricteduse@qw.com";
		data[1][1] = "abcd1234";
		data[1][2] = "restricteduse@qw.com";

		
		return data1;
	}
****/
		
	@DataProvider
	public Object [][]getData1() throws IOException {
		// Row stands for how many different data sets test should run
		// Column stands for how many values per each data set.
		
		String filepath = System.getProperty("user.dir")+"/src/main/java/resources/C2ImportSchoolSample.csv";
		
		
		ArrayList<HashMap<String, String >> data1 = base.getcsvData(filepath);
		
		
		
		//Iterator<Object[]> newdata = (Iterator<Object[]>) base.getcsvData(filepath);
		
		int numberofRows = getcsvRowCount(filepath);
		
		Object[][] testData = new Object[numberofRows-1][1];
		
		for (int j = 0 ; j<numberofRows-1; j++) {
					
		HashMap<String, String >tempmap =data1.get(j);
			System.out.println("J value :"+ j + tempmap);
		}
		
		for (int j = 0 ; j<numberofRows-1; j++) {
			testData[j][0] = data1.get(j);
			
			System.out.println("J value :"+ j + testData[j][0]);
		}
		
		
		
		/**
		Object[][] data = new Object[2][3];
		// 0th row
		data[0][0] = "nonrestricteduse@qw.com";
		data[0][1] = "abcd1234";
		data[0][2] = "nonrestricteduse@qw.com";
		// 1st row
		data[1][0] = "restricteduse@qw.com";
		data[1][1] = "abcd1234";
		data[1][2] = "restricteduse@qw.com";

		****/
		
		return testData;
	}	
	
	
	@AfterMethod
	public void teardown() {
		driver.close();
		log.debug("broswer closed");
		driver = null;
		log.debug("driver made null");
		log.info("********end of HomePage test 2**********");
	}

}
