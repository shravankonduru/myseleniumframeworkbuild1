package Academy;

import java.io.IOException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import pageObjects.LandingPage;
import pageObjects.LoginPage;
import resources.base;
// Adding logs
// Generating html reports
// Screenshots on failure
// Jenkins integration

public class ValidateNavBar extends base {
	public static Logger log = LogManager.getLogger(ValidateNavBar.class.getName());

	@BeforeTest
	public void initialize() throws IOException {
		log.info("********start of ValidateNavBar test 1 **********");
		driver = initializeDriver();
		log.debug("driver initialized");
		driver.get(prop.getProperty("url"));
		log.debug("Url launched successfully");
		extTest = extReports.createTest("ValidateNavBar");

	}

	@Test
	public void ValidateNavBar() throws IOException {
//		driver = initializeDriver();
//		driver.get(prop.getProperty("url"));
		// one is inheritance
		// creating object to that class and invoke methods of it

		LandingPage lpobj = new LandingPage(driver);
		log.debug("landing page obj created");
		// should always pass arguments which results in true
		Assert.assertTrue(lpobj.getNavbar().isDisplayed(), "Navbar is displayed");
		log.info("Assertion successfull");
		// should always pass an arguments which results in false
		Assert.assertFalse(!lpobj.getNavbar().isDisplayed());
	}

	@AfterTest
	public void teardown() {
		driver.close();
		log.debug("browser closed");
		driver = null;
		log.debug("driver made null");
		log.info("********end of ValidateNavBar test 1**********");
	}

}
