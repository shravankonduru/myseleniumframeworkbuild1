package Academy;

import java.io.IOException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import pageObjects.LandingPage;
import pageObjects.LoginPage;
import resources.base;

public class ValidateTitle extends base {
	public static Logger log = LogManager.getLogger(ValidateTitle.class.getName());

	@BeforeTest
	public void initialize() throws IOException {
		log.info("********start of ValidateTitle test 3**********");
		driver = initializeDriver();
		log.info("Driver is initialized");
		driver.get(prop.getProperty("url"));
		log.debug("URL is launched");
		extTest = extReports.createTest("ValidateTitle");
	}

	@Test
	public void ValidateTitle() throws IOException {
		// one is inheritance
		// creating object to that class and invoke methods of it
		log.info("In basePageNavigation Method");
		LandingPage lpobj = new LandingPage(driver);
		// compare the text from the browser with actual text.
		log.debug("first Assertion");
		Assert.assertEquals(lpobj.getTtile().getText(), "FEATURED COURSES57687");
		//base.extTest.fail();
		log.debug("second Assertion");
		Assert.assertTrue(lpobj.getNavbar().isDisplayed(), "Navbar is displayed");
		log.info("Assertion successful");
	}

	@AfterTest
	public void teardown() {
		log.debug("in tear down method");
		driver.close();
		driver = null;
		log.debug("driver made null");
		log.info("********end of ValidateTitle test 3**********");
	}

}
