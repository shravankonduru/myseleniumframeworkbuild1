package Academy;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import pageObjects.LandingPage;
import pageObjects.LoginPage;
import pageObjects.NOPCommerceLandingPage;
import pageObjects.NOPCommerceRegistrationPage;
import resources.base;

public class NOPComRegisterUserTest extends base {
	public static Logger log = LogManager.getLogger(NOPComRegisterUserTest.class.getName());
	
	@BeforeSuite
	public void setupextentobjs() {
		base.initializeExtentTest();
	}
	
	@AfterSuite
	public void flushupextentobjs() {
		base.EndReports();
	}

	@BeforeMethod
	public void initialize() throws IOException {
		log.info("********start of HomePage test 2**********");
		driver = initializeDriver();
		driver.get(prop.getProperty("url"));
		extTest = extReports.createTest("Register User");
	}

	@Test(dataProvider = "getData1")
	public void ValidateLogin(HashMap<String, String > data1) throws IOException {
		
		log.debug("******* Start ******Itereation for UserNo: " + data1.get("UserNo")+"**********");
		
		log.debug("UserNo : "+data1.get("UserNo"));
		log.debug("FirstName : "+data1.get("FirstName"));
		log.debug("LastName : "+data1.get("LastName"));
		log.debug("Email : "+data1.get("Email"));
		log.debug("Username : "+data1.get("Username"));
		log.debug("Role : "+data1.get("Role"));
		log.debug("Country : "+data1.get("Country"));
		log.debug("ReceiveNewsletters : "+data1.get("ReceiveNewsletters"));
		log.debug("Password : "+data1.get("Password"));
		log.debug("******* End ******Itereation for UserNo: " + data1.get("UserNo")+"**********");	
		
		
		
		NOPCommerceLandingPage lndPg = new NOPCommerceLandingPage(driver);

		log.debug("Verify Landing Page is  Displayed");
		Assert.assertTrue(lndPg.getLndPgValidaitonElement().isDisplayed(), "Landing Page is Displayed");
		log.info("Assertion successful");
		
	
		log.debug("Click on Resgiration link");
		lndPg.getRegister().click();
		 
		
		NOPCommerceRegistrationPage Rgstnpg =  new NOPCommerceRegistrationPage(driver);
		
		
		
		
//Registration Page validation
		log.debug("Validation Start: Verify if Registration Page is  Displayed");
		if(Rgstnpg.getRegistrationHeadingElement().isDisplayed()) {
			log.debug("Validation completed: Registration Page is  Displayed as expected");	
			extTest.pass("Validation completed: Registration Page is  Displayed as expected");
		}else {
			log.error("Validation Failed: Registration Page is not Displayed as expected."
					+ " Registration Element is not displayed");
			extTest.fail("Validation Failed: Registration Page is not Displayed as expected."
					+ " Registration Element is not displayed");
		
		}

//Enter Form details
		log.debug("Enter ");
		

		
	//	lp.getemail().sendKeys(UserName);
	//	log.debug("provided  username" + UserName);
		
		
		
		extTest.pass("Username is entered successfully");
	//	lp.getpassword().sendKeys(Password);
		extTest.pass("password is entered successfully");
		log.debug("provided  Password");
		//lp.getloginBtn().click();
		log.debug("Click on Sing button");
	//	System.out.println("third text: " + text);
		log.info("inside homepage test");

	}

	/**
	@DataProvider
	public Object [][] getData() throws IOException {
		// Row stands for how many different data sets test should run
		// Column stands for how many values per each data set.
		
		String filepath = System.getProperty("user.dir")+"/src/main/java/resources/RegistrationTestData.csv";
		
		
		ArrayList<HashMap<String, String >> data1 = base.getcsvData(filepath);
		
		/**
		Object[][] data = new Object[2][3];
		// 0th row
		data[0][0] = "nonrestricteduse@qw.com";
		data[0][1] = "abcd1234";
		data[0][2] = "nonrestricteduse@qw.com";
		// 1st row
		data[1][0] = "restricteduse@qw.com";
		data[1][1] = "abcd1234";
		data[1][2] = "restricteduse@qw.com";

		
		return data1;
	}
****/
		
	@DataProvider
	public Object [][]getData1() throws IOException {
		// Row stands for how many different data sets test should run
		// Column stands for how many values per each data set.
		
		String filepath = System.getProperty("user.dir")+"/src/main/java/resources/RegistrationTestData.csv";
		
		
		ArrayList<HashMap<String, String >> data1 = base.getcsvData(filepath);
		
		
		
		//Iterator<Object[]> newdata = (Iterator<Object[]>) base.getcsvData(filepath);
		
		int numberofRows = getcsvRowCount(filepath);
		
		Object[][] testData = new Object[numberofRows-1][1];
		
		for (int j = 0 ; j<numberofRows-1; j++) {
					
		HashMap<String, String >tempmap =data1.get(j);
			System.out.println("J value :"+ j + tempmap);
		}
		
		for (int j = 0 ; j<numberofRows-1; j++) {
			testData[j][0] = data1.get(j);
			
			System.out.println("J value :"+ j + testData[j][0]);
		}
		
		
		
		/**
		Object[][] data = new Object[2][3];
		// 0th row
		data[0][0] = "nonrestricteduse@qw.com";
		data[0][1] = "abcd1234";
		data[0][2] = "nonrestricteduse@qw.com";
		// 1st row
		data[1][0] = "restricteduse@qw.com";
		data[1][1] = "abcd1234";
		data[1][2] = "restricteduse@qw.com";

		****/
		
		return testData;
	}	
	
	
	@AfterMethod
	public void teardown() {
		driver.close();
		log.debug("broswer closed");
		driver = null;
		log.debug("driver made null");
		log.info("********end of HomePage test 2**********");
	}

}
