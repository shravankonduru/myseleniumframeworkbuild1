package Academy;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import pageObjects.LandingPage;
import pageObjects.LoginPage;
import resources.base;

public class ReadCVS2HashMap extends base {
	public static Logger log = LogManager.getLogger(ReadCVS2HashMap.class.getName());
	
//	@BeforeSuite
//	public void setupextentobjs() {
//		base.initializeExtentTest();
//	}
//	
//	@AfterSuite
//	public void flushupextentobjs() {
//		base.EndReports();
//	}

	@BeforeTest
	public void initialize() throws IOException {
		log.info("********start of ValidateTitle test 3**********");
		//driver = initializeDriver();
		base.initializeExtentTest();
//		log.info("Driver is initialized");
//		driver.get(prop.getProperty("url"));
//		log.debug("URL is launched");
		extTest = extReports.createTest("ReadCVS2HashMap");
	}

	@Test
	public void ReadCVS2HashMap() throws IOException {
		
		//Users/dhivya/Documents/shravan/Selenium/Project/src/main/java/resources/C2ImportSchoolSample.csv
		String filepath = System.getProperty("user.dir")+"/src/main/java/resources/C2ImportSchoolSample.csv";
		
		int count = base.getcsvRowCount(filepath);
		log.debug("row count :"+ count);
		
		ArrayList<HashMap<String, String >> datamap1 = base.getcsvData(filepath);
		log.debug("csv data"+ datamap1);
		
		System.out.println(datamap1);
	}
	
	

	@AfterTest
	public void teardown() {
		log.debug("in tear down method");
//		driver.close();
//		driver = null;
		log.debug("driver made null");
		log.info("********end of ValidateTitle test 3**********");
		base.EndReports();
	}

}
