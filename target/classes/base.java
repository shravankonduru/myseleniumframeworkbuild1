package resources;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.support.ui.Select;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.aventstack.extentreports.reporter.configuration.Theme;

import io.github.bonigarcia.wdm.WebDriverManager;



public class base {
	public static Logger log = LogManager.getLogger(base.class.getName());
	public static WebDriver driver;
	public static Select simpleDropDwnElemnt;
	public Properties prop;
	
	public static ExtentHtmlReporter extHtmlReporter;
	public static ExtentReports extReports;
	public static ExtentTest extTest;
	
	public static void initializeExtentTest() {
		log.debug("Entry : Method - initialize ExtentTest");
		log.debug("system path" +System.getProperty("user.dir"));
		extHtmlReporter = new  ExtentHtmlReporter(System.getProperty("user.dir")+"\\test-output\\mytestreports.html");
		
		extHtmlReporter.config().setDocumentTitle("Automation Report"); //set Title of the report
		extHtmlReporter.config().setReportName("Funtional Report"); //  Name of the report
		extHtmlReporter.config().setTheme(Theme.DARK);
		log.debug("extHtmlReporter Object created");
		
		extReports = new ExtentReports();
		extReports.attachReporter(extHtmlReporter);
		extReports.setSystemInfo("HostName", "LocalHost");
		extReports.setSystemInfo("OS", "MacOS");
		extReports.setSystemInfo("DeveloperName", "Shravan");
		extReports.setSystemInfo("Browser", "Chrome");
		log.debug("extHtmlReporter Object created");
		log.debug("Exit : Method - initialize ExtentTest");
	}
	
	public static void EndReports() {
		
		extReports.flush();
	}
	
	public Select createSimpleDropdwnElement(WebElement DropdownElement)
	{
		simpleDropDwnElemnt = new Select(DropdownElement);
		return simpleDropDwnElemnt;
	}

	public  WebDriver initializeDriver() throws IOException
	{
		prop = new Properties();
		log.debug("system path " +System.getProperty("user.dir")+ "\\src\\main\\java\\resources\\data.properties");
		//log.debug("system path " +System.getProperty("user.home")+ "\\src\\main\\java\\resources\\data.properties");
		FileInputStream fis = new FileInputStream(System.getProperty("user.dir")+ "\\src\\main\\java\\resources\\data.properties");
		
		prop.load(fis);
		String browserName=prop.getProperty("browser");
		System.out.println("browser Name: "+browserName);
		
		if(browserName.equalsIgnoreCase("chrome")) {
			// Using WebDriver Mangaer to no need to SetProperty for driver.exe files.
			//System.setProperty("webdriver.chrome.driver", "/Users/dhivya/Documents/shravan/Selenium/drivers/chromedriver");
			// Use below line of code for setup of driver.exe files for WebDriver specific to a Browser.
			 WebDriverManager.chromedriver().setup();
			driver = new ChromeDriver();
		}
		else if (browserName.equalsIgnoreCase("firefox"))
		{
			WebDriverManager.firefoxdriver().setup();
			driver = new FirefoxDriver();
		}
		else if (browserName.equalsIgnoreCase("IE"))
		{
			
			WebDriverManager.iedriver().setup();
			driver = new InternetExplorerDriver();
			
		}
		else if(browserName.equalsIgnoreCase("edge"))
		{
			WebDriverManager.edgedriver().setup();
			driver = new EdgeDriver();
		}
		
		//implicit wait that applies to all places in framework
		//minimum time scripts waits before test case is declared as fail
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		
		return driver;
		
		
	}
	
		/****
		 * To get Screenshots method 1
		 * @param TestName
		 * @throws IOException
		 */
	public void getScreenshots(String TestName) throws IOException
	{
		File src = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
		// Use below line and file path format in Mac OS 
		//FileUtils.copyFile(src, new File("/Users/dhivya/Documents/shravan/Selenium/Screenshots/Errors/"+TestName+"screenshot.png"));
		
		//Use below Window OS
		log.debug("system path " +System.getProperty("user.home")+ "\\shravan\\Selenium\\Screenshots\\Errors");
		FileUtils.copyFile(src, new File(System.getProperty("user.home")+ "\\shravan\\Selenium\\Screenshots\\Errors\\"+TestName+"screenshot.png"));
		
	}
	
	public static String getScreenshot(String TestName) throws IOException {
		String ScreenshotName = TestName;
		String dateName = new SimpleDateFormat("yyyyMMddhhmmss").format(new Date());
		TakesScreenshot ts = (TakesScreenshot)driver;
		File source = ts.getScreenshotAs(OutputType.FILE);
		
		// After execution, you could see a folder "FailedTestsScreenshots" under Src folder
		//String destination = System.getProperty("user.home")+"/Screenshots/Extent" + ScreenshotName +dateName + ".png";
		String destination = System.getProperty("user.home")+"\\Screenshots\\Extent" + ScreenshotName +dateName + ".png";
		File finalDestination = new File(destination);
		FileUtils.copyFile(source, finalDestination);
		return destination;
		
	}
	
	public static int getcsvRowCount(String Filepath) throws IOException {
		BufferedReader bufferedReader = new BufferedReader(new FileReader(Filepath));
	     String input;
	     int count = 0;
	     while((input = bufferedReader.readLine()) != null)
	     {
	         count++;
	     }

	     System.out.println("Count : "+count);
		return count;
	}
	
	public static ArrayList<HashMap<String, String >> getcsvData(String Filepath) throws IOException{
		ArrayList<HashMap<String, String >> datamap = new ArrayList<HashMap<String, String >> ();
		//HashMap<String, String > tempHashMap = new HashMap<String, String >();
		String temprowdata = null;
		int numberofRows = getcsvRowCount(Filepath);
		//BufferedReader bufferedReader = new BufferedReader(new FileReader(Filepath));
		
		String header [] = null;
		String temprow [] = null;
		
		BufferedReader bufferedReader = new BufferedReader(new FileReader(Filepath));
		for( int i =1 ; i <= numberofRows ; i++) {
			//BufferedReader bufferedReader = new BufferedReader(new FileReader(Filepath));
			if(i==1) {
			temprowdata = bufferedReader.readLine();
			header = temprowdata.split(",");	
			continue;
			}
			
				temprowdata = bufferedReader.readLine();
				temprow = temprowdata.split(",");
				HashMap<String, String > tempHashMap = new HashMap<String, String >();
				int colcount = temprow.length;
				 for(int k=0; k<=colcount-1 ; k++) {
					 
					 tempHashMap.put(header[k],temprow[k]);
				 }
			 
			 
			 datamap.add(tempHashMap);
			 //tempHashMap.clear();
			
			 System.out.println("print test datamap : I : " +i +",  value: "+tempHashMap);
				
			 System.out.println("print test datamap :" +datamap);
			 tempHashMap = null;
		}
		
		System.out.println("print test datamap :" +datamap);
		return datamap;
		
	}
	 
//	public static Object [] getcsvData1(String Filepath) throws IOException{
//		Object[] datamap ;
//		
//		
//		
//		HashMap<String, String > tempHashMap = new HashMap<String, String >();
//		String temprowdata = null;
//		int numberofRows = getcsvRowCount(Filepath);
//		HashMap<String, String > obj[]= new HashMap<String, String >[numberofRows];
//		
//		BufferedReader bufferedReader = new BufferedReader(new FileReader(Filepath))
//		
//		String header [] = null;
//		String temprow [] = null;
//		
//		for( int i =1 ; i <= numberofRows ; i++) {
//			
//			if(i==1) {
//			temprowdata = bufferedReader.readLine();
//			header = temprowdata.split(",");	
//			continue;
//			}
//			temprowdata = bufferedReader.readLine();
//			temprow = temprowdata.split(",");
//			int colcount = temprow.length;
//			 for(int k=0; k<=colcount-1 ; k++) {
//				 tempHashMap.put(header[k],temprow[k]);
//			 }
//			 
//			
//		}
//		
//		
//		return datamap;
//		
//	}
	
	
}
